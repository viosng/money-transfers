Stub Money Transfer 
=========

## Description
The REST service provides two API methods - **registerTransfer** and **/result/{transferId}** for registering and getting 
results respectively.



## Build
**mvn clean package**

## Run
* with config in **external-config.yml** file - **java -jar money-transfers-1.0-SNAPSHOT.jar server external-config.yml**
* without config - **java -jar money-transfers-1.0-SNAPSHOT.jar server**

## API

### 1.Register transfer
* Description: register the transfer request and return the unique transfer id that can be used to retrieve result
* Method: **POST**
* Required header: **Content-Type: application/json**
* Path: **/registerTransfer**
* Payload: 
    **{
      "currency": "USD", 
      "from": "first", // 'from' account id
      "to": "second",// 'to' account id
      "amount": "3.45645"
    }**
* Result example:
**{
"status": "OK",
"result": 4,
"message": "registered"
}**

### 2.Get result
* Description: wait and return the result of transfer. In case of error return error message in "message" field and Error status
* Method: **GET**
* Required header: **Content-Type: application/json**
* Path: **/result/{transferId}**** 
**Example /result/4**
* Result example 1: **{
                    "status": "OK",
                    "result": "transferred",
                    "message": null
                    }**
* Result example 1: **{
                    "status": "ERROR",
                    "result": null,
                    "message": "Account first has less amount of Currency{name='USD'} than 3"
                    }**
                                                       
### 3.Get account data
 * Description: get account data - for testing only
 * Method: **GET**
 * Required header: **Content-Type: application/json**
 * Path: **/account/{id}** 
 **Example /account/first**
 * Result example 1: **{
                     "status": "OK",
                     "result": {
                     "id": "first",
                     "currencyAmounts": {
                     "Currency{name='EUR'}": 2000,
                     "Currency{name='USD'}": 10,
                     "Currency{name='RUB'}": 3000
                     }
                     },
                     "message": null
                     }**