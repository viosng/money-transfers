package me.app.money.transfer.service.impl;

import javafx.util.Pair;
import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Result;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.*;

/**
 * Created by viosng on 20.07.2017.
 */
public class SimpleLockServiceTest {

    private SimpleLockService simpleLockService;

    @Before
    public void setUp() throws Exception {
        simpleLockService = new SimpleLockService();
    }

    @Test
    public void handleExclusively() throws Exception {
        Account account = new Account("id", Collections.emptyMap());
        AtomicLong counter = new AtomicLong();
        int N = 200, sleep = 10;
        long time = System.currentTimeMillis();
        for(int i = 0; i < N; i++) {
            Result<String> result = simpleLockService.handleExclusively(account, () -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                   throw new RuntimeException(e);
                }
                counter.incrementAndGet();
                return Result.success("OK");
            });
            if (result.getStatus() == Result.Status.ERROR) {
               fail(result.getMessage());
           }
        }
        assertTrue("Wrong locking", System.currentTimeMillis() - time >= N * sleep);
        assertEquals("Wrong processing", N, counter.get());
    }

    @Test
    public void orderAccounts() throws Exception {
        Account one = new Account("1", Collections.emptyMap());
        Account two = new Account("2", Collections.emptyMap());
        Pair<Account, Account> pair = simpleLockService.orderAccounts(new Pair<>(one, two));
        assertEquals(one, pair.getKey());
        assertNotEquals(one, pair.getValue());
        assertEquals(two, pair.getValue());
        assertNotEquals(two, pair.getKey());

        pair = simpleLockService.orderAccounts(new Pair<>(two, one));
        assertEquals(one, pair.getKey());
        assertNotEquals(one, pair.getValue());
        assertEquals(two, pair.getValue());
        assertNotEquals(two, pair.getKey());
    }

}