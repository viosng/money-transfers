package me.app.money.transfer.service.impl;

import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Currency;
import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import me.app.money.transfer.service.AccountService;
import me.app.money.transfer.service.TransferService;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static me.app.money.transfer.data.Result.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by viosng on 20.07.2017.
 */
public class SimpleTransferServiceTest {

    @Before
    public void setUp() throws Exception {
        AccountService accountService = new SimpleAccountService(Arrays.asList(
                new Account("first", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of("usd"), BigDecimal.valueOf(1000));
                    put(Currency.of("EUR"), BigDecimal.valueOf(2000));
                    put(Currency.of("rub"), BigDecimal.valueOf(3000));
                }}),
                new Account("second", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of("usd"), BigDecimal.valueOf(10000));
                    put(Currency.of("EUR"), BigDecimal.valueOf(3));
                }}),
                new Account("third", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of("EUR"), BigDecimal.valueOf(20000000));
                    put(Currency.of("rub"), BigDecimal.valueOf(300000));
                }})
        ));
    }

    @Test
    public void concurrentTransfers() throws Exception {
        int transfersCount = 100_000;
        String currency = "usd";
        AccountService accountService = new SimpleAccountService(Arrays.asList(
                new Account("a2", Collections.emptyMap()),
                new Account("a1", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of(currency), BigDecimal.valueOf(transfersCount));
                }}),
                new Account("a3", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of(currency), BigDecimal.valueOf(transfersCount));
                }})
        ));
        TransferService transferService = new SimpleTransferService(
                new SimpleTransferProcessingService(accountService, new SimpleLockService()), ForkJoinPool.commonPool());

        Consumer<String> transferProcessing = from -> {
            List<Result<Long>> transfers = IntStream.range(0, transfersCount)
                    .mapToObj(i -> transferService.registerTransfer(new TransferDescription(currency, from, "a2", BigDecimal.ONE)))
                    .collect(Collectors.toList());
            assertTrue(transfers.stream().allMatch(r -> r.getStatus() == OK));
            assertTrue(transfers.stream().allMatch(r -> r.getResult() != null));
            assertTrue(transfers.stream().map(r -> transferService.getResult(r.getResult())).allMatch(r -> r.getStatus() == OK));
        };

        CompletableFuture.allOf(
                CompletableFuture.runAsync(() -> transferProcessing.accept("a1")),
                CompletableFuture.runAsync(() -> transferProcessing.accept("a3"))
        ).get();

        assertEquals(2 * transfersCount, accountService.findAccount("a2").get().getAmount(Currency.of(currency)).intValue());
    }
}