package me.app.money.transfer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import me.app.money.transfer.checks.SimpleCheck;
import me.app.money.transfer.config.AppInjector;
import me.app.money.transfer.resources.TransferResource;

public class Main extends Application<Configuration> {

    @Override
    public String getName() {
        return "money-registerTransfer";
    }

    @Override
    public void initialize(Bootstrap<Configuration> bootstrap) {
    }

    @Override
    public void run(Configuration configuration, Environment environment) {
        Injector injector = Guice.createInjector(new AppInjector());

        environment.healthChecks().register("template", new SimpleCheck());
        environment.jersey().register(injector.getInstance(TransferResource.class));
    }

    public static void main(String[] args) throws Exception {
        new Main().run(args);
    }
}
