package me.app.money.transfer.resources;

import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import me.app.money.transfer.service.AccountService;
import me.app.money.transfer.service.TransferService;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by viosng on 19.07.2017.
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private final AccountService accountService;

    private final TransferService transferService;

    @Inject
    public TransferResource(@NotNull AccountService accountService, @NotNull TransferService transferService) {
        this.accountService = accountService;
        this.transferService = transferService;
    }

    @POST
    @Path("/registerTransfer")
    public Result<Long> registerTransfer(TransferDescription transferDescription) {
        if (transferDescription == null) return Result.error("empty transfer description");
        return transferService.registerTransfer(transferDescription);
    }

    @GET
    @Path("/result/{transferId}")
    public Result getResult(@PathParam("transferId") Long transferId) {
        if (transferId == null) return Result.error("empty transfer id");
        return transferService.getResult(transferId);
    }

    @GET
    @Path("/account/{id}") // for testing purposes only
    public Result getAccount(@PathParam("id") String accountId) {
        if (accountId == null) return Result.error("empty account id");
        return Result.success(accountService.findAccount(accountId));
    }
}