package me.app.money.transfer.config;

import com.google.inject.AbstractModule;
import me.app.money.transfer.service.AccountService;
import me.app.money.transfer.service.LockService;
import me.app.money.transfer.service.TransferProcessingService;
import me.app.money.transfer.service.TransferService;
import me.app.money.transfer.service.impl.SimpleAccountService;
import me.app.money.transfer.service.impl.SimpleLockService;
import me.app.money.transfer.service.impl.SimpleTransferProcessingService;
import me.app.money.transfer.service.impl.SimpleTransferService;

/**
 * Created by viosng on 20.07.2017.
 */
public class AppInjector extends AbstractModule{
    @Override
    protected void configure() {
        bind(TransferService.class).to(SimpleTransferService.class);
        bind(LockService.class).to(SimpleLockService.class);
        bind(AccountService.class).to(SimpleAccountService.class);
        bind(TransferProcessingService.class).to(SimpleTransferProcessingService.class);
    }
}
