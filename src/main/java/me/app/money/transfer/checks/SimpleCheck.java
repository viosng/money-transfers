package me.app.money.transfer.checks;

import com.codahale.metrics.health.HealthCheck;

/**
 * Created by viosng on 19.07.2017.
 */
public class SimpleCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}