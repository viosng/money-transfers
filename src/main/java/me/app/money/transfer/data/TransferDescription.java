package me.app.money.transfer.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by viosng on 19.07.2017.
 */
public final class TransferDescription {
    private final String currency, from, to;
    private final BigDecimal amount;

    @JsonCreator
    public TransferDescription(@JsonProperty("currency") String currency,
                               @JsonProperty("from")  String from,
                               @JsonProperty("to") String to,
                               @JsonProperty("amount") BigDecimal amount) {
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransferDescription{" +
                "currency='" + currency + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", amount=" + amount +
                '}';
    }
}
