package me.app.money.transfer.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by viosng on 20.07.2017.
 */
public class Account {

    @NotNull
    private final String id;

    @JsonProperty
    private final ConcurrentHashMap<Currency, BigDecimal> currencyAmounts = new ConcurrentHashMap<>();

    public Account(@NotNull String id, @NotNull Map<Currency, BigDecimal> currencyAmounts) {
        this.id = id;
        this.currencyAmounts.putAll(currencyAmounts);
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public BigDecimal getAmount(@NotNull Currency currency) {
        return currencyAmounts.getOrDefault(currency, BigDecimal.ZERO);
    }

    public void addAmount(@NotNull Currency currency, @NotNull BigDecimal amount) {
        currencyAmounts.compute(currency,
                (k, v) -> Optional.ofNullable(v)
                                .orElse(BigDecimal.ZERO)
                                .add(amount)
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", currencyAmounts=" + currencyAmounts +
                '}';
    }
}
