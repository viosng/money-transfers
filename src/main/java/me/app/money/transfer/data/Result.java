package me.app.money.transfer.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by viosng on 19.07.2017.
 */
public class Result<T> {

    public enum Status {
        OK, ERROR
    }

    @NotNull
    private final Status status;

    @Nullable
    private final T result;

    @Nullable
    private final String message;

    private Result(@NotNull Status status, @Nullable T result, @Nullable String message) {
        this.status = status;
        this.result = result;
        this.message = message;
    }

    @NotNull
    public static <R> Result<R> success(@Nullable R result, @Nullable String message) {
        return new Result<>(Status.OK, result, message);
    }

    @NotNull
    public static <R> Result<R> success(@Nullable R result) {
        return new Result<>(Status.OK, result, null);
    }

    public static <R> Result<R> error(@NotNull Throwable e) {
        return new Result<>(Status.ERROR, null, e.getMessage());
    }

    public static <R> Result<R> error(@NotNull String message) {
        return new Result<>(Status.ERROR, null, message);
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    @Nullable
    public T getResult() {
        return result;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status=" + status +
                ", result=" + result +
                ", message='" + message + '\'' +
                '}';
    }
}
