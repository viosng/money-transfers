package me.app.money.transfer.data;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Created by viosng on 20.07.2017.
 */
public class Currency {

    @NotNull
    private final String name;

    public Currency(@NotNull String name) {
        this.name = name.toUpperCase();
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(name, currency.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Currency{" +
                "name='" + name + '\'' +
                '}';
    }

    @NotNull
    public static Currency of(@NotNull String currency) {
        return new Currency(currency);
    }
}
