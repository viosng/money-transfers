package me.app.money.transfer.service;

import me.app.money.transfer.data.Account;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Created by viosng on 20.07.2017.
 */
public interface AccountService {
    @NotNull
    Optional<Account> findAccount(@NotNull String id);
}
