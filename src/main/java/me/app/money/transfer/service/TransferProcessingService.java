package me.app.money.transfer.service;

import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import org.jetbrains.annotations.NotNull;

/**
 * Created by viosng on 20.07.2017.
 */
public interface TransferProcessingService {

    Result<String> process(@NotNull TransferDescription transferDescription);
}
