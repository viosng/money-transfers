package me.app.money.transfer.service.impl;

import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import me.app.money.transfer.service.TransferProcessingService;
import me.app.money.transfer.service.TransferService;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by viosng on 20.07.2017.
 */

@Singleton
public class SimpleTransferService implements TransferService{
    private final static long TIMEOUT_SECONDS = 60 * 60;
    private final static AtomicLong nextTransferId = new AtomicLong();
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final TransferProcessingService transferProcessingService;
    private final Map<Long, Future<Result<String>>> results = new ConcurrentHashMap<>();
    private final ForkJoinPool forkJoinPool;

    @Inject
    public SimpleTransferService(TransferProcessingService transferProcessingService,
                                 ForkJoinPool forkJoinPool) {
        this.transferProcessingService = transferProcessingService;
        this.forkJoinPool = forkJoinPool;
    }

    @NotNull
    @Override
    public Result<Long> registerTransfer(@NotNull TransferDescription transferDescription) {
        log.info("Transfer registered: {}", transferDescription);
        long transferId = nextTransferId.getAndIncrement();
        try {
            results.put(transferId,
                    CompletableFuture
                            .supplyAsync(() -> transferProcessingService.process(transferDescription), forkJoinPool)
                            .handle((result, ex) -> {
                                Result<String> res = result;
                                if (ex != null) res = Result.error(ex);
                                log.info("Transfer : {} completed: {}", transferDescription, res);
                                return res;
                            }));
            return Result.success(transferId, "registered");
        } catch (Exception e) {
            return Result.error(e);
        }
    }

    @NotNull
    @Override
    public Result<String> getResult(long transferId) {
        try {
            return results.getOrDefault(transferId,
                    CompletableFuture.completedFuture(Result.error("The transfer with id = " + transferId + " wasn't found")))
                    .get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
        } catch (Exception e) {
            return Result.error(e);
        }
    }
}
