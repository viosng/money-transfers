package me.app.money.transfer.service;

import javafx.util.Pair;
import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Result;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

/**
 * Created by viosng on 20.07.2017.
 */
public interface LockService {

    <T> Result<T> handleExclusively(@NotNull Account account, @NotNull Supplier<Result<T>> supplier);

    @NotNull
    Pair<Account, Account> orderAccounts(@NotNull Pair<Account, Account> pair);
}
