package me.app.money.transfer.service.impl;

import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Currency;
import me.app.money.transfer.service.AccountService;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by viosng on 20.07.2017.
 */
@Singleton
public class SimpleAccountService implements AccountService {

    private final Map<String, Account> accountMap = new ConcurrentHashMap<>();

    public SimpleAccountService() {
        // sample accounts
        this(Arrays.asList(
                new Account("first", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of("usd"), BigDecimal.valueOf(10));
                    put(Currency.of("EUR"), BigDecimal.valueOf(2000));
                    put(Currency.of("rub"), BigDecimal.valueOf(3000));
                }}),
                new Account("second", new HashMap<Currency, BigDecimal>() {{
                    put(Currency.of("usd"), BigDecimal.valueOf(10000));
                    put(Currency.of("EUR"), BigDecimal.valueOf(3));
                }}),
                new Account("third", new HashMap<Currency, BigDecimal>(){{
                    put(Currency.of("EUR"), BigDecimal.valueOf(20000000));
                    put(Currency.of("rub"), BigDecimal.valueOf(300000));
                }})
        ));
    }

    SimpleAccountService(@NotNull List<Account> accounts) {
        accounts.forEach(account -> accountMap.put(account.getId(), account));
    }

    @NotNull
    @Override
    public Optional<Account> findAccount(@NotNull String id) {
        return Optional.ofNullable(accountMap.get(id));
    }
}
