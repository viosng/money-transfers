package me.app.money.transfer.service.impl;

import javafx.util.Pair;
import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Currency;
import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import me.app.money.transfer.service.AccountService;
import me.app.money.transfer.service.LockService;
import me.app.money.transfer.service.TransferProcessingService;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;

/**
 * Created by viosng on 20.07.2017.
 */
@Singleton
public class SimpleTransferProcessingService implements TransferProcessingService {

    private final AccountService accountService;
    private final LockService lockService;

    @Inject
    protected SimpleTransferProcessingService(AccountService accountService, LockService lockService) {
        this.accountService = accountService;
        this.lockService = lockService;
    }

    @Override
    public Result<String> process(@NotNull TransferDescription transferDescription) {
        Account from = getAccount(transferDescription.getFrom());
        Account to = getAccount(transferDescription.getTo());
        Currency currency = new Currency(transferDescription.getCurrency());
        BigDecimal amount = transferDescription.getAmount();
        return amount.compareTo(BigDecimal.ZERO) >= 0
                ? handleTransfer(from, to, currency, amount)
                : handleTransfer(to, from, currency, amount.negate());
    }

    private Result<String> validateTransfer(@NotNull Account from,
                                            @NotNull Account to,
                                            @NotNull Currency currency,
                                            @NotNull BigDecimal amount) {
        if (from.getAmount(currency).compareTo(amount) < 0) {
            return Result.error("Account " + from.getId() + " has less amount of " + currency + " than " + amount);
        }
        return Result.success("validated");
    }

    private Result<String> handleTransfer(@NotNull Account from,
                                          @NotNull Account to,
                                          @NotNull Currency currency,
                                          @NotNull BigDecimal amount) {
        Pair<Account, Account> ordered = lockService.orderAccounts(new Pair<>(from, to));
        return lockService.handleExclusively(ordered.getKey(),
                () -> lockService.handleExclusively(ordered.getValue(), () -> {
                    Result<String> validateResult = validateTransfer(from, to, currency, amount);
                    if (validateResult.getStatus() != Result.Status.OK) return validateResult;
                    from.addAmount(currency, amount.negate());
                    to.addAmount(currency, amount);
                    return Result.success("transferred");
                }));
    }

    private Account getAccount(@NotNull String id) {
        return accountService.findAccount(id)
                .orElseThrow(() -> new IllegalArgumentException("Can't find user with id = " + id));
    }
}
