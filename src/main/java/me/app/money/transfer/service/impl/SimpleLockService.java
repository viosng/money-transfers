package me.app.money.transfer.service.impl;

import javafx.util.Pair;
import me.app.money.transfer.data.Account;
import me.app.money.transfer.data.Result;
import me.app.money.transfer.service.LockService;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

/**
 * Created by viosng on 20.07.2017.
 */
@Singleton
public class SimpleLockService implements LockService {
    private final static long TRY_LOCK_TIMEOUT_SECONDS = 60;

    private final Map<Account, ReentrantLock> lockObjects = new ConcurrentHashMap<>();

    @Override
    public <T> Result<T> handleExclusively(@NotNull Account account, @NotNull Supplier<Result<T>> supplier) {
        ReentrantLock lock = lockObjects.computeIfAbsent(account, k -> new ReentrantLock());
        try {
            if (!lock.tryLock(TRY_LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS)) return Result.error("Process timeout. Try again");
            try {
                return supplier.get();
            } finally {
                lock.unlock();
            }
        } catch (InterruptedException e) {
            return Result.error(e);
        }
    }

    @Override
    public @NotNull Pair<Account, Account> orderAccounts(@NotNull Pair<Account, Account> pair) {
        return pair.getKey().getId().compareTo(pair.getValue().getId()) <= 0
                ? pair
                : new Pair<>(pair.getValue(), pair.getKey());
    }
}
