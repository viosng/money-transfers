package me.app.money.transfer.service;

import me.app.money.transfer.data.Result;
import me.app.money.transfer.data.TransferDescription;
import org.jetbrains.annotations.NotNull;

/**
 * Created by viosng on 20.07.2017.
 */
public interface TransferService {

    @NotNull
    Result<Long> registerTransfer(@NotNull TransferDescription transferDescription);

    @NotNull
    Result<String> getResult(long transferId);
}
